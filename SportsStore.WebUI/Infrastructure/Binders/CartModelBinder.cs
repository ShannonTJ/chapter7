﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore1.Entities;

namespace SportsStore.WebUI.Infrastructure.Binders
{
    public class CartModelBinder : IModelBinder
    {
        private const string sessionKey = "Cart";

        //controller context provides access to all info that the controller class has
        //model binding context gives ingo about the model object being built
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            //get cart from session
            Cart cart = null;

            if (controllerContext.HttpContext.Session != null)
            {
                cart = (Cart)controllerContext.HttpContext.Session[sessionKey];
            }

            //create the Cart if there wasn't one in the session data
            if (cart == null)
            {
                cart = new Cart();

                if(controllerContext.HttpContext.Session != null)
                {
                    controllerContext.HttpContext.Session[sessionKey] = cart;
                }
            }

            //return cart
            return cart;
        }
    }

}