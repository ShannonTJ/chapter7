﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore1.Abstract;
using SportsStore1.Entities;
using SportsStore.WebUI.Models;

namespace SportsStore.WebUI.Controllers
{
    public class ProductController : Controller
    {
        private IProductsRepository repository;
        public int PageSize = 4;

        //constructor with dependency on IProductRepository
        //inject dependency for the product repo when controller is instantiated
        public ProductController(IProductsRepository productRepository)
        {
            this.repository = productRepository;
        }

        //Action method displays 1st page of products when invoked with no argument
        public ViewResult List(string category, int page = 1)
        {
            //PageSize = how many products per page
            //Get Product objects, order by ProductID (primary key), skip products before the current page, take number of products specify by PageSize
            //return View(repository.Products.OrderBy(p => p.ProductID).Skip((page - 1)*PageSize).Take(PageSize));

            ProductsListViewModel model = new ProductsListViewModel
            {
                //if category is not null, only the Products with a matching Category are selected
                Products = repository.Products
                .Where(p => category == null || p.Category == category)
                .OrderBy(p => p.ProductID)
                .Skip((page - 1) * PageSize)
                .Take(PageSize),

                //if category is selected, return the number of items in that category
                //if not, return the total number of products
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null?
                        repository.Products.Count() :
                        repository.Products.Where(e => e.Category == category).Count()
                },
                CurrentCategory = category
            };

            return View(model);
        }
    }
}