﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore1.Abstract;

namespace SportsStore.WebUI.Controllers
{
    public class NavController : Controller
    {
        private IProductsRepository repository;

        //dependency
        public NavController(IProductsRepository repo)
        {
            repository = repo;
        }

        // GET: Nav
        //linq query to obtain a list of categories from the repo
        public PartialViewResult Menu(string category = null)
        {
            //value is the current category
            ViewBag.SelectedCategory = category;

            IEnumerable<string> categories = repository.Products
                .Select(x => x.Category)
                .Distinct()
                .OrderBy(x => x);

            //string viewName = horizontalLayout ? "MenuHorizontal" : "Menu";

            //always call FlexMenu
            //removes duplication in Menu and MenuHorizontal
            return PartialView("FlexMenu", categories);
        }
    }
}