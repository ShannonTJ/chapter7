﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore1.Abstract;
using SportsStore1.Entities;

namespace SportsStore.WebUI.Controllers
{
    public class AdminController : Controller
    {
        private IProductsRepository repository;

        //declare dependency on the IPR interface
        public AdminController(IProductsRepository repo)
        {
            repository = repo;
        }

        // GET: Admin
        //default view for the action, passes the set of products in the database
        public ViewResult Index()
        {
            return View(repository.Products);
        }

        public ViewResult Create()
        {
            //use the Edit view
            //(Edit view has all-empty fields bc of new product object)
            return View("Edit", new Product());
        }

        [HttpPost]
        public ActionResult Delete(int productId)
        {
            Product deletedProduct = repository.DeleteProduct(productId);

            if(deletedProduct != null)
            {
                TempData["message"] = string.Format("{0} was deleted", deletedProduct.Name);
            }
            return RedirectToAction("Index");
        }

        //finds product with corresponding ID
        //passes object to the View method
        public ViewResult Edit(int productId)
        {
            Product product = repository.Products.FirstOrDefault(p => p.ProductID == productId);
            return View(product);
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            //check if data values are valid
            if(ModelState.IsValid)
            {
                //save changes to repo
                repository.SaveProduct(product);
                //store message with TempData feature
                TempData["message"] = string.Format("{0} has been saved", product.Name);
                //return the user to the list of products
                return RedirectToAction("Index");
            }

            else
            {
                //problem with the data values
                //display edit screen so user can fix problems
                return View(product);
            }
        }
    }
}