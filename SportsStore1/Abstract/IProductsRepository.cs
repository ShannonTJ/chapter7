﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore1.Entities;

namespace SportsStore1.Abstract
{
    public interface IProductsRepository
    {
        //allows caller to obtain a sequence of Products
        //don't have to specify where data is stored/retrieved
        IEnumerable<Product> Products { get; }

        void SaveProduct(Product product);

        Product DeleteProduct(int productID);
    }
}
