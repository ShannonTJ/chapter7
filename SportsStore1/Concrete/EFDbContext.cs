﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore1.Entities;
using System.Data.Entity;

namespace SportsStore1.Concrete
{
    public class EFDbContext : DbContext
    {
        //automatically defines a property for each table in the db
        //name of property specifies the table
        //type parameter specifies the model type
        public DbSet<Product> Products { get; set; }
    }
}
