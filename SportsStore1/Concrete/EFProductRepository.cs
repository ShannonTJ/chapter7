﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsStore1.Abstract;
using SportsStore1.Entities;

namespace SportsStore1.Concrete
{
    //repository class
    //implements IProductRepository interface
    //uses instance of EFDbContext to retrieve data
    //uses Entity Framework
    public class EFProductRepository : IProductsRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Product> Products
        {
            get { return context.Products; }
        }

        public void SaveProduct(Product product)
        {
            //Add product to repository if ID is 0
            if(product.ProductID == 0)
            {
                context.Products.Add(product);
            }

            //Else apply the changes to an existing entry in the database
            else
            {
                Product dbEntry = context.Products.Find(product.ProductID);

                if(dbEntry != null)
                {
                    dbEntry.Name = product.Name;
                    dbEntry.Description = product.Description;
                    dbEntry.Price = product.Price;
                    dbEntry.Category = product.Category;
                }
            }

            context.SaveChanges();

        }

        public Product DeleteProduct(int productID)
        {
            Product dbEntry = context.Products.Find(productID);

            if (dbEntry != null)
            {
                context.Products.Remove(dbEntry);
                context.SaveChanges();
            }

            return dbEntry;
        }
    }
}
